;============================================================
; Profibus Device Database of ProSoft Technology
; Model : PDPMV1
; Description : ProSoft Technology Profibus master
; Language : English
; Date : 040315
; Author : HMS Industrial Networks
;
; Revision log:
; 1.00 - First release
; 1.01 - Updated Model_Name, Added keywords for NetTool-PB
; 1.02 - Changed for ProSoft
;=================================================================
#Profibus_DP
;
;=================================================================
; General keywords
;=================================================================
;
GSD_Revision        = 3
Vendor_Name         = "ProSoft Technology"
Model_Name          = "ProSoft"      ; PDPMV1	
Revision            = "1.02"         ; Version number of the GSD-file
Ident_Number        = 0x18F0
Protocol_Ident      = 0              ; DP protocol
Station_Type        = 1              ; Master station
FMS_supp            = 0              ; FMS not supported
Hardware_Release    = "1.0"
Software_Release    = "1.5"
;
9.6_supp   = 1
19.2_supp  = 1
45.45_supp = 1
93.75_supp = 1
187.5_supp = 1
500_supp   = 1
1.5M_supp  = 1
3M_supp    = 1
6M_supp    = 1
12M_supp   = 1
;
MaxTsdr_9.6   = 15
MaxTsdr_19.2  = 15
MaxTsdr_45.45 = 15
MaxTsdr_93.75 = 15
MaxTsdr_187.5 = 15
MaxTsdr_500   = 26
MaxTsdr_1.5M  = 75
MaxTsdr_3M    = 150
MaxTsdr_6M    = 300
MaxTsdr_12M   = 600
;
Redundancy        = 0           ; Redundancy is not supported
Repeater_Ctrl_Sig = 2           ; TTL-level
24V_Pins          = 0           ; Not connected
;
Implementation_Type = "ASPC2"
;
;=================================================================
; DP Master (Class 1) related keywords
;=================================================================
;
Master_Freeze_Mode_supp = 1     ; Freeze mode supported
Master_Sync_Mode_supp = 1       ; Sync mode supported
Master_Fail_Safe_supp = 1       ; Fail Safe supported
;
Download_supp = 0               ; Function Download not supported
Upload_supp = 0                 ; Function Upload not supported
Act_Para_Brct_supp = 0          ; Function Act_Para_Brct not supported
Act_Param_supp = 0              ; Function Act_Param not supported
Max_MPS_Length = 0              ; Max size for storing the Master Parameter Set
;
Max_Lsdu_MS = 244          ; Max telegram length for Master-Slave communication
Max_Lsdu_MM = 244          ; Max telegram length for Master-Master communication
;
Min_Poll_Timeout = 1       ; Max time for processing a Master-Master function
;
Trdy_9.6   = 11
Trdy_19.2  = 11
Trdy_45.45 = 11
Trdy_93.75 = 11
Trdy_187.5 = 11
Trdy_500   = 11
Trdy_1.5M  = 11
Trdy_3M    = 11
Trdy_6M    = 11
Trdy_12M   = 11
;
Tqui_9.6   = 0
Tqui_19.2  = 0
Tqui_45.45 = 0
Tqui_93.75 = 0
Tqui_187.5 = 0
Tqui_500   = 0
Tqui_1.5M  = 0
Tqui_3M    = 3
Tqui_6M    = 6
Tqui_12M   = 9
;
Tset_9.6   =  1
Tset_19.2  =  1
Tset_45.45 = 95
Tset_93.75 =  1
Tset_187.5 =  1
Tset_500   =  1
Tset_1.5M  =  1
Tset_3M    =  4
Tset_6M    =  8
Tset_12M   = 16
;
LAS_Len = 126            ; Max number of entries in the list of active stations
;
Tsdi_9.6   =   1
Tsdi_19.2  =   1
Tsdi_45.45 =   1
Tsdi_93.75 =   3
Tsdi_187.5 =   5
Tsdi_500   =  13
Tsdi_1.5M  =  39
Tsdi_3M    =  78
Tsdi_6M    = 156
Tsdi_12M   = 312
;
Max_Slaves_supp = 125          ; Max number of slave stations
;
Max_Master_Input_Len = 244     ; Max input data length per slave
Max_Master_Output_Len = 244    ; Max output data length per slave
Max_Master_Data_Len = 488      ; Max total data length per slave
;
;=================================================================
; DPV1 Master (Class 1) related keywords
;=================================================================
;
DPV1_Master = 1
DPV1_Conformance_Class = 2          ; Supports MSAC1_Read/Write, MSAL_Alarm_Ack
C1_Master_Read_Write_supp = 1
;
Master_DPV1_Alarm_supp = 1
Master_Diagnostic_Alarm_supp = 1
Master_Process_Alarm_supp = 1
Master_Pull_Plug_Alarm_supp = 1
Master_Status_Alarm_supp = 1
Master_Update_Alarm_supp = 1
Master_Manufacturer_Specific_Alarm_supp = 1
;
Master_Extra_Alarm_SAP_supp = 1     ; MSAL_Alarm_Ack on SAP50 is possible
Master_Alarm_Sequence_Mode = 7      ; 32 Alarms in total may be active
Master_Alarm_Type_Mode_supp = 1
;
;========================================================================
; User related keywords
;========================================================================
;
Adressing_Mode = 0                              ; 0 = Byte ; 1 = Word
Adressing_Format = 0                            ; 0 = Motorola ; 1 = Intel
Adressing_Offset_Inp = 0                        ; Input address offset
Adressing_Offset_Outp = 0                       ; Output address offset
Dual_Port_Ram_Type = 1                          ; 0 = Standard mode ; 1 = Extended mode
Adressing_Size_Inp = 1536                       ; Maximum input data length (bytes)
Adressing_Size_Outp = 1536                      ; Maximum output data length (bytes)



