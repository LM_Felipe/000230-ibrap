;//:+++++++++++++    SCHNEIDER ELECTRIC       +++++++++++++
;//:          Copyright (C) 2005, All rights reserved
;//:
;//:  SUBSYSTEM:    Advantys STB  
;//:
;//:  MODULE:       Profibus Coupler GSD (Geraete Stammdaten Datei)
;//:			for
;//:			STB NDP 1010
;//:
;//:---------------------------------------------------------------------------
;//:  Revision: 1.05   Date: 24. April 2005  
;//:---------------------------------------------------------------------------
;//:
;//:  ATTENTION:
;//:  ==========
;//:  Changes to this file can cause configuration or communication problems.
;//:  This file is compatible to the firmware of the device.
;//:
;//:---------------------------------------------------------------------------
;//:     
;//:---------------------------------------------------------------------------

#Profibus_DP

;------------------------------------------------------------------------------
; Unit Definition List
;------------------------------------------------------------------------------
GSD_Revision        = 2
Vendor_Name         = "Telemecanique"
Model_Name          = "STB NDP 1010"
Revision            = "1.0"
Ident_Number        = 0x063F                     ; PNO-Identnumber 
Protocol_Ident      = 0                          ; Profibus-DP
Station_Type        = 0                          ; DP-Slave
FMS_supp            = 0                          ; 
Hardware_Release    = "1.0"
Software_Release    = "1.01"
9.6_supp            = 1
19.2_supp           = 1
45.45_supp          = 1
93.75_supp          = 1
187.5_supp          = 1
500_supp            = 1
1.5M_supp           = 1
3M_supp             = 1
6M_supp             = 1
12M_supp            = 1
MaxTsdr_9.6         = 15
MaxTsdr_19.2        = 15
MaxTsdr_45.45       = 15
MaxTsdr_93.75       = 15
MaxTsdr_187.5       = 15
MaxTsdr_500         = 15
MaxTsdr_1.5M        = 25
MaxTsdr_3M          = 50
MaxTsdr_6M          = 100
MaxTsdr_12M         = 200

Redundancy          = 0                          ; no redundant transmission
Repeater_Ctrl_Sig   = 2                          ; signal CNTR-P (TTL level) connected
24V_Pins            = 0                          ; signals M24V and P24V not connected

; DP Slave Specifications
Freeze_Mode_supp    = 1
Sync_Mode_supp      = 0
Auto_Baud_supp      = 1
Set_Slave_Add_supp  = 0
Min_Slave_Intervall = 6                         ; 600us
Modular_Station     = 1
Max_Module          = 12                      
Max_Input_Len       = 240
Max_Output_Len      = 240
Max_Data_Len        = 240
Max_Diag_Data_Len   = 32                            
Slave_Family        = 3

User_Prm_Data_Len = 1
User_Prm_Data = 0                                             
;---------------------- Kopfparameter -------------------------

Bitmap_Device = "STB_R"
Bitmap_Diag   = "STB_D"
Bitmap_SF     = "STB_S"

;------------------------------------------------------------------------------
; STB Discrete Input Modules
;------------------------------------------------------------------------------

Module = "STB_DAI_5230"    0x41,0x0,0x0B                            ; 115VAC IN 2pt 3wire Standard
EndModule  

Module = "STB_DAI_5260"    0x41,0x0,0x0D                            ; 115VAC IN 2pt 3wire Isolated Standard
EndModule                                               

Module = "STB_DAI_7220"    0x41,0x0,0x1B                            ; 230VAC IN 2pt 3wire Standard
EndModule                                                 

Module = "STB_DDI_3230"    0x41,0x0,0x01                            ; 24VDC IN 2pt sink 4wire Standard
EndModule 
                                          
Module = "STB_DDI_3420"    0x41,0x0,0x09                            ; 24VDC IN 4pt sink 3wire Standard
EndModule 

Module = "STB_DDI_3425"    0x41,0x0,0x89                            ; 24VDC IN 4pt sink 3wire Basic
EndModule 

Module = "STB_DDI_3610"    0x41,0x01,0x03                           ; 24VDC IN 6pt sink 2wire Standard
EndModule   

Module = "STB_DDI_3615"    0x41,0x0,0x83                           ; 24VDC IN 6pt sink 2wire Basic
EndModule 

Module = "STB_DDI_3725"    0x41,0x40,0x87                           ; 24VDC IN 16pt sink 2wire Basic
EndModule  
;------------------------------------------------------------------------------
; STB Discrete Output Modules
;------------------------------------------------------------------------------

Module = "STB_DAO_5260"    0xC1,0x0,0x0,0x04                        ; 115VAC OUT 2pt source 2A Isolated Standard                                                                 
EndModule 

Module = "STB_DAO_8210"    0xC1,0x0,0x0,0x12                        ; 115/230VAC OUT 2pt source 2A Standard
EndModule                                                                                           

Module = "STB_DDO_3200"    0xC1,0x0,0x0,0x08                        ; 24VDC OUT 2pt source .5A Standard
EndModule  

Module = "STB_DDO_3230"    0xC1,0x0,0x0,0x0E                        ; 24VDC OUT 2pt source 2A Standard
EndModule
 
Module = "STB_DDO_3410"    0xC1,0x0,0x0,0x0A                        ; 24VDC OUT 4pt source .5A Standard
EndModule  

Module = "STB_DDO_3415"    0x81,0x0,0x8A                            ; 24VDC OUT 4pt source .25A Basic
EndModule 

Module = "STB_DDO_3600"    0xC1,0x0,0x1,0x10                        ; 24VDC OUT 6pt source .5A Standard
EndModule

Module = "STB_DDO_3605"    0x81,0x0,0x90                        ; 24VDC OUT 6pt source .25A Basic
EndModule

Module = "STB_DDO_3705"    0x81,0x40,0x82                           ; 24VDC OUT 16pt source .5A Standard
EndModule 

Module = "STB_DRA_3290"    0xC1,0x0,0x0,0x20                        ; Relay OUT 2pt 7A Standard
EndModule                                                           

Module = "STB_DRC_3210"    0xC1,0x0,0x0,0x0C                        ; Relay OUT 2pt 2A Standard                                                                   
EndModule

;------------------------------------------------------------------------------
; STB Analog Input Modules
;------------------------------------------------------------------------------

Module = "STB_ACI_1225"    0x41,0x41,0xC2                  ; Current 2ch 4-20mA 10bit Basic
EndModule

Module = "STB_ACI_1230"    0x41,0x42,0x42                  ; Current 2ch 0-20mA 12bit Standard
EndModule

Module = "STB_ART_0200"    0x41,0x43,0x41             ; RTD/TC/mV 2ch 15bit+sign Standard
EndModule

Module = "STB_AVI_1255"    0x41,0x41,0xC1             ; Voltage 2ch 0-10V 10bit Basic
EndModule

Module = "STB_AVI_1270"    0x41,0x42,0x40             ; Voltage 2ch -/+10V 1bit+sign Standard
EndModule

Module = "STB_AVI_1275"    0x41,0x41,0xC0             ; Voltage 2ch -/+10V 9bit+sign Basic
EndModule
;------------------------------------------------------------------------------
; STB Analog Output Modules
;------------------------------------------------------------------------------

Module = "STB_ACO_1210"    0xC1,0x41,0x40,0x44             ; Current 2ch 0-20mA 12bit Standard
EndModule

Module = "STB_ACO_1225"    0x81,0x41,0xC4                  ; Current 2ch 4-20mA 12bit Basic
EndModule

Module = "STB_AVO_1250"    0xC1,0x41,0x40,0x4A             ; Volatge 2ch -/+10V 11bit+sign Standard
EndModule   
   
Module = "STB_AVO_1255"    0x81,0x41,0xCB                  ; Volatge 2ch 0-10V 10bit Basic
EndModule 

Module = "STB_AVO_1265"    0x81,0x41,0xCA                  ; Volatge 2ch -/+10V 9bit+sign Basic
EndModule 
  
;------------------------------------------------------------------------------
; STB Special purpose Modules
;------------------------------------------------------------------------------                                               
Module = "STB_EHC_3020"    0xC1,0x05,0x07,0x17            ; High Speed Counter Multimode 40kHZ
EndModule                                                 

Module = "STB_EPI_1145"    0xC1,0x0,0x05,0x16             ; Tego Power 16in/8out Parallel Interface
EndModule                                                 

Module = "STB_EPI_2145"    0xC1,0x0,0x04,0x18             ; Tesys U 12in/8out parallel Interface
EndModule    

;------------------------------------------------------------------------------
; Byte Sized Modules for Advantys STB Analog I/O 
;------------------------------------------------------------------------------
Module = "Byte-Modul STB_ACI_1225"    0x41,0x03,0xC2           ; Current 2ch 4-20mA 10bit Basic
EndModule

Module = "Byte-Modul STB_ACI_1230"    0x41,0x05,0x42           ; Current 2ch 0-20mA 12bit Standard
EndModule

Module = "Byte-Modul STB_ART_0200"    0x41,0x07,0x41           ; RTD/TC/mV 2ch 15bit+sign Standard
EndModule

Module = "Byte-Modul STB_AVI_1255"    0x41,0x03,0xC1             ; Voltage 2ch 0-10V 10bit Basic
EndModule

Module = "Byte-Modul STB_AVI_1270"    0x41,0x05,0x40           ; Voltage 2ch -/+10V 1bit+sign Standard
EndModule

Module = "Byte-Modul STB_AVI_1275"    0x41,0x03,0xC0                  ; Voltage 2ch -/+10V 9bit+sign Basic
EndModule

Module = "Byte-Modul STB_ACO_1210"    0xC1,0x03,0x01,0x44             ; Current 2ch 0-20mA 12bit Standard
EndModule

Module = "Byte-Modul STB_ACO_1225"    0x81,0x03,0xC4                  ; Current 2ch 4-20mA 12bit Basic
EndModule

Module = "Byte-Modul STB_AVO_1250"    0xC1,0x03,0x01,0x4A             ; Volatge 2ch -/+10V 11bit+sign Standard
EndModule
   
Module = "Byte-Modul STB_AVO_1255"    0x81,0x03,0xCB                  ; Volatge 2ch 0-10V 10bit Basic
EndModule 

Module = "Byte-Modul STB_AVO_1265"    0x81,0x03,0xCA                  ; Volatge 2ch -/+10V 9bit+sign Basic
EndModule 
