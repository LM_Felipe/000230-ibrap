;//:+++++++++++++    SCHNEIDER ELECTRIC   +++++++++++++
;//:         Copyright (C) 2001, Schneider Electric  All rights reserved.
;//:
;//:SUBSYSTEM:	Tego Power
;//:
;//:MODULE:	TEG_PROF.GSD (Geraete Stammdaten Datei)
;//:
;//:--------------------------------------------------------------------------------------------
;//:$Revision: B1.1 $  $Date: 2003/03/06  $
;//:--------------------------------------------------------------------------------------------
;//:DESCRIPTION: PROFIBUS-DP Tego-Power-Koppler/SPC 3 bis 1.5 MBaud
;//:--------------------------------------------------------------------------------------------
;//:
;//:$Log: TEG_PROF.GSD $
;//:Revision B1.1  2003/03/06  R. Laetzel
;//:	formale �nderungen
;//:	Bitmap-Namen ge�ndert
;//:--------------------------------------------------------------------------------------------

#Profibus_DP
GSD_Revision = 1		; erw.  GSD-Datei wird unterstuetzt (gem. PNO-Richtlinie 14.12.95)
Vendor_Name = "Telemecanique"		; Herstellername
Model_Name = "Tego Power APP1 CPF.."	; Bezeichnung des DP-Geraetes
Revision = "B1.1"    	                     	; Ausgabestand des DP-Geraetes
Ident_Number = 0x048B	                      	; Geraetetyp des DP-Geraetes
Protocol_Ident = 0                     	     	; Protokollkennung PROFIBUS-DP
Station_Type = 0                      	      	; DP-Slave
FMS_supp = 0                        	        	; DP-Geraet
Hardware_Release = "A01"                    	; Hardware-Ausgabestand
Software_Release = "B1-FD"                  	; Software-Ausgabestand
;
9.6_supp = 1                                	;   9.6  kBaud
19.2_supp = 1                               	;  19.2  kBaud
93.75_supp = 1                              	;  93.75 kBaud
187.5_supp = 1                              	; 187.5  kBaud
500_supp = 1                                	; 500    kBaud
1.5M_supp = 1                               	;   1.5  MBaud
3M_supp = 1                                 	;     3  MBaud
6M_supp = 1                                 	;     6  MBaud
12M_supp = 1                                	;    12  MBaud
MaxTsdr_9.6 = 60
MaxTsdr_19.2 = 60
MaxTsdr_93.75 = 60
MaxTsdr_187.5 = 60
MaxTsdr_500 = 100
MaxTsdr_1.5M = 150
MaxTsdr_3M = 250
MaxTsdr_6M = 450
MaxTsdr_12M = 800
;
Redundancy = 0                    	; keine redundante Uebertragung
Repeater_Ctrl_Sig = 0                       	; Steckersignal CNTR-P nicht angeschlossen
24V_Pins = 0			; Steckersignale M24V und P24V nicht angeschlossen
Implementation_type = "SPC3"	; ASIC SPC3 wird verwendet

;-------------------- Slave spezifische Werte --------------

Freeze_Mode_supp = 1		; Freeze-Mode wird unterstueetzt
Sync_Mode_supp = 1		; Sync-Mode wird unterstuetzt
Auto_Baud_supp = 1		; Autom. Baudratenerkennung
Set_Slave_Add_supp = 0		; Set_Slave_Add wird nicht unterstuetzt
Min_Slave_Intervall    = 1		;
;
Modular_Station   = 1		; Modulare Station
Max_Module         = 1
Max_Output_Len = 3
Max_Input_Len    = 4
Max_Data_Len     = 7
;
Fail_Safe = 1			; Datentelegramm ohne Daten im CLEAR-Fall
Max_Diag_Data_Len = 64		; maximale Laenge der Diagnosedaten
Modul_Offset = 0			; erstes Modul im Konfigurationstool
Slave_Family = 0			; Geraete-Familie
;
User_Prm_Data_Len = 15	                  		; 15 Byte User-Parameter-Daten
User_Prm_Data = 0x00,0x00,0x00,0x00,0x00,\
                0x00,0x00,0x00,0x00,0x63,\
                0x00,0x10,0x00,0x00,0x00			 ; Defaultwerte fuer User-Prm-Data
;
Bitmap_Device="TEGO_R"           ; Bitmap fuer Normalzustand
Bitmap_Diag="TEGO_D"               ; Bitmap fuer Diagnose
Bitmap_SF="TEGO_S"
	
;-------------------- Klemmentypen -------------------------

Module="APP1 CPF0/3 - 8 Out/16 In" 0x20,0x11
EndModule
Module = "APP1 CPF2/5 - 24 Out/32 In" 0x22,0x13
EndModule

;---------------------- Dateiende -----------------------
