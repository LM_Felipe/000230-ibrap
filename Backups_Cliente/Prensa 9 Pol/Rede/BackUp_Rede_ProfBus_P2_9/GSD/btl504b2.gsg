;================================================================
;  GSD-File for Micropulse
;  Company:  Balluff GmbH
;  Version:  10.02
;  Contact:  Service-Center
;  Tel.   :  07158/173-370
;  Fax.   :  07158/173-691
;  eMail  :  profibus@balluff.de
;================================================================

#Profibus_DP

;
;---parameter text---
;
;octet9
; Parameter text f�r Messrichtung
PrmText=1
Text(0)="Minimalwert an Steckerseite"
Text(1)="Maximalwert an Steckerseite"
EndPrmText

; Parameter text f�r Klasse 4 Funktionalit�t
PrmText=2
Text(0)="Ausgeschaltet"
Text(1)="Eingeschaltet"
EndPrmText

; Parameter text f�r Offset Kontrolle G1_XIST1
PrmText=3
Text(0)="G1_XIST1 mit Offset"
Text(1)="G1_XIST1 ohne Offset"
EndPrmText

; Parameter text f�r Skalierungsfunktion
PrmText=4
Text(0)="Ausgeschaltet"
Text(1)="Eingeschaltet"
EndPrmText

;Parameter text for velocity 
PrmText=21
;Text(3)="Umdrehungen pro Minute"
Text(2)="Schritte/10 ms"
Text(1)="Schritte/100 ms"
Text(0)="Schritte/1000 ms"
EndPrmText

; Prameter text for User Diagnostic
PrmText=103
Text(0)="Ausgeschaltet"
Text(1)="Eingeschaltet"
EndPrmText

;
;---parameter selection---
;
;octet 12
; User Parameter "Code sequence"
ExtUserPrmData=1 "Messrichtung"
Bit(0) 0 0-1
Prm_Text_Ref=1
EndExtUserPrmData

;User Parameter "Class 4 Functionality Enable"
ExtUserPrmData=2 "Klasse 4 Funktionalitaet"
Bit(1) 1 0-1
Prm_Text_Ref=2
EndExtUserPrmData

;User Parameter "Preset Control"
ExtUserPrmData=3 "Offset Kontrolle G1_XIST1"
Bit(2) 1 0-1
Prm_Text_Ref=3
EndExtUserPrmData

;User Parameter "Scaling Function Control"
ExtUserPrmData=4 "Skalierungsfunktion"
Bit(3) 1 0-1
Prm_Text_Ref=4
EndExtUserPrmData

;User Parameter "Ext. Diag Enable"
ExtUserPrmData=103 "Erweiterte Diagnose"
Bit(4) 1 0-1
Prm_Text_Ref=103
EndExtUserPrmData

;User Parameter "Measuring Units per Res"
;octed 13-16
ExtUserPrmData=12 "Schrittweite [0.001um]"
Unsigned32 0 0-0xFFFFFFFF
EndExtUserPrmData

;User Parameter "Total Measuring Range"
;octed 17-20
ExtUserPrmData=6 "Messbereich in Schrittweite"
Unsigned32 0 0-0xFFFFFFFF
EndExtUserPrmData

;User Parameter "max. tol.failures of lifesign"
;octed 21
ExtUserPrmData=20 "max.tol.Anz.Fehler Lebensz."
Unsigned8 15 0-0xFF
EndExtUserPrmData

;User Parameter "velocity measuring unit"
;octed 22
ExtUserPrmData=21 "Geschwindigkeitsaufloesung"
Unsigned8 2 0-0x02
Prm_Text_Ref=21
EndExtUserPrmData


;
;start with manufacturer specific entries
;
;User Parameter "Diag Time FMM"
;octet 41
ExtUserPrmData=11 "Diagnosezeit fuer FMM"
Unsigned8 0 0-0xFF
EndExtUserPrmData

GSD_Revision=5

;
;---communication behaviour---
;
Vendor_Name         = "Balluff GmbH"
Model_Name          = "BTL5-T110"
Revision            = "Version 10.02"
Ident_Number        = 0x04B2
Protocol_Ident      = 0	; PROFIBUS DP
Station_Type        = 0 ; DP SLAVE
FMS_supp            = 0	; No FMS/DP mixed device
Hardware_Release    = "V02.00"
Software_Release    = "V03.04"
9.6_supp            = 1
19.2_supp           = 1
93.75_supp          = 1
187.5_supp          = 1
500_supp            = 1
1.5M_supp           = 1
3M_supp             = 1
6M_supp             = 1
12M_supp            = 1
MaxTsdr_9.6         = 60
MaxTsdr_19.2        = 60
MaxTsdr_93.75       = 60
MaxTsdr_187.5       = 60
MaxTsdr_500         = 100
MaxTsdr_1.5M        = 150
MaxTsdr_3M          = 250
MaxTsdr_6M          = 450
MaxTsdr_12M         = 800
Redundancy          = 0		; No
Repeater_Ctrl_Sig   = 0		; No
24V_Pins            = 0		; Pin M24V and P24V not connected

Implementation_Type = "VPC3+C"
Bitmap_Device       = "BTL5_st"
Bitmap_Diag         = "BTL5_nc"
Bitmap_SF           = "BTL5_nm"

; Info-text for "PROFIdrive single axis servo drive, app class 4, 1 encoder"
Info_Text               = "PROFIdrive einachsiger Servoantrieb, App Klasse 4, 1 Wegaufnehmer"
DPV1_Slave              = 1
C1_Read_Write_supp      = 1
C2_Read_Write_supp      = 1
C1_Max_Data_Len         = 240
C2_Max_Data_Len         = 240
C1_Response_Timeout     = 10
C2_Response_Timeout     = 10
C1_Read_Write_required  = 0  
C2_Read_Write_required  = 0  
C2_Max_Count_Channels   = 1
Max_Initiate_PDU_Length = 52
DPV1_Data_Types         = 0
WD_Base_1ms_supp        = 1
Check_Cfg_Mode          = 0

Ident_Maintenance_supp = 1 					
; GSD R4 slave extensions

Prm_Block_Structure_supp= 1
Prm_Block_Structure_req = 1

; Parameters for Isochronmode 

Isochron_Mode_supp      = 1
Isochron_Mode_required  = 0
TBASE_DP                = 1500 ; 125�s
TDP_MIN                 = 16		; 2 ms   
TDP_MAX                 = 256  ; 32 ms
;T_PLL_W_MAX             = 12; 1 �s
T_PLL_W_MAX             = 120; 10 �s
TBASE_IO                = 1500 ; 125�s
TI_MIN                  = 1		; 125�s
TO_MIN                  = 1		; 125�s


;OrderNumber="BTL5-T110-MXXXX-X-S103"

;
;---slave specific values---
;
Fail_safe           = 1
Freeze_Mode_supp    = 1
Sync_Mode_supp      = 1
Auto_Baud_supp      = 1
Set_Slave_Add_supp  = 1
Slave_Family        = 7@Linear@Absolute

Min_Slave_Intervall = 0x0006  ; 600�s

Modular_Station     = 1
Max_Module          = 1

Max_Input_Len       = 244
Max_Output_Len      = 04
Max_Data_Len        = 244

Max_Diag_Data_Len   = 90


Channel_Diag (16)       =   "Maximal Frequenz ueberschritten"
Channel_Diag_Help (16)  =   "Geschwindigkeit reduzieren"

Channel_Diag (17)       =   "Light control reserve reached"
Channel_Diag_Help (17)  =   "Wegaufnehmer tauschen, damit keine falschen Messwerte entstehen"

Channel_Diag (18)       =   "CPU watchdog reset"
Channel_Diag_Help (18)  =   "Interner Firmware Fehler oder EMV Problem"

Channel_Diag (19)       =   "Lebenszeitlimit erreicht"
Channel_Diag_Help (19)  =   "Wegaufnehmer tauschen, damit keine falschen Messwerte entstehen"

Channel_Diag (20)       =   "Versorgungspannung zu gering"
Channel_Diag_Help (20)  =   "Wegaufnehmer tauschen, damit keine falschen Messwerte entstehen oder Versorgungspannung kontrollieren"

Channel_Diag (21)       =   "Referenzpunkt nicht erreicht"
Channel_Diag_Help (21)  =   "Positionsgeber manuell bewegen um den referenzpunkt zu erreichen"

Channel_Diag (22)       =   "Positionfehler"
Channel_Diag_Help (22)  =   "Es liegt ein interner Fehler vor und der Wegaufnehmer ist nicht in der Lage gueltige Positionswerte zu liefern"

Channel_Diag (23)       =   "Fehler waehrend Inbetriebnahme"
Channel_Diag_Help (23)  =   "Es liegt ein interner Fehler vor und der Wegaufnehmer ist nicht in der Lage gueltige Positionswerte zu liefern, Wegaufnehmer wechseln"

Channel_Diag (24)       =   "Speicherfehler"
Channel_Diag_Help (24)  =   "Es liegt ein interner Fehler vor und der Wegaufnehmer ist nicht in der Lage gueltige Positionswerte zu liefern, Wegaufnehmer wechseln"

Channel_Diag (31)       =   "Noch nicht synchronisiert"
Channel_Diag_Help (31)  =   "Der Wegaufnehmer hat sich noch nicht synchronisiert und ist noch nicht in der Lage gueltige Positionswerte zu liefern"

UnitDiagType        = 129
X_Unit_Diag_Bit(0x18) = "E:Position nicht gueltig"
X_Unit_Diag_Bit(0x19) = "E:Versorgungsspannung"
X_Unit_Diag_Bit(0x1a) = "E:Stromaufnahme zu hoch"
X_Unit_Diag_Bit(0x1b) = "E:Inbetriebnahmediagnose"
X_Unit_Diag_Bit(0x1c) = "E:Speicherfehler"

X_Unit_Diag_Bit(0x20) = "S:Arbeitsbereich Invertierung"
X_Unit_Diag_Bit(0x21) = "S:Klasse 4 eingeschaltet"
X_Unit_Diag_Bit(0x22) = "S:Diagnose eingeschaltet"
X_Unit_Diag_Bit(0x23) = "S:Skalierung eingeschaltet"

X_Unit_Diag_Bit(0x81) = "E:Uebertemperatur"
X_Unit_Diag_Bit(0x84) = "E:Lebenszeit Limit Warnung"


X_Unit_Diag_Area=344-359
X_Value(0x0000) = "S:16 Positionsgeber verfuegbar"   
X_Value(0x8000) = "S:15 Positionsgeber verfuegbar"   
X_Value(0xc000) = "S:14 Positionsgeber verfuegbar"   
X_Value(0xe000) = "S:13 Positionsgeber verfuegbar"   
X_Value(0xf000) = "S:12 Positionsgeber verfuegbar"   
X_Value(0xf800) = "S:11 Positionsgeber verfuegbar"
X_Value(0xfc00) = "S:10 Positionsgeber verfuegbar" 
X_Value(0xfe00) = "S:9 Positionsgeber verfuegbar"
X_Value(0xff00) = "S:8 Positionsgeber verfuegbar"
X_Value(0xff80) = "S:7 Positionsgeber verfuegbar"
X_Value(0xffc0) = "S:6 Positionsgeber verfuegbar"
X_Value(0xffe0) = "S:5 Positionsgeber verfuegbar"
X_Value(0xfff0) = "S:4 Positionsgeber verfuegbar"
X_Value(0xfff8) = "S:3 Positionsgeber verfuegbar"
X_Value(0xfffc) = "S:2 Positionsgeber verfuegbar"
X_Value(0xfffe) = "S:1 Positionsgeber verfuegbar"
X_Value(0xffff) = "E:Kein Positionsgeber verfuegbar"
X_Unit_Diag_Area_End


X_Unit_Diag_Area=390-391
X_Value(0)  = "S:Pos4 im Arbeitsbereich"
X_Value(1)  = "S:Pos4 kleiner untere Grenze"
X_Value(2)  = "S:Pos4 groesser obere Grenze"
X_Unit_Diag_Area_End

X_Unit_Diag_Area=388-389
X_Value(0)  = "S:Pos3 im Arbeitsbereich"
X_Value(1)  = "S:Pos3 kleiner untere Grenze"
X_Value(2)  = "S:Pos3 groesser obere Grenze"
X_Unit_Diag_Area_End

X_Unit_Diag_Area=386-387
X_Value(0)  = "S:Pos2 im Arbeitsbereich"
X_Value(1)  = "S:Pos2 kleiner untere Grenze"
X_Value(2)  = "S:Pos2 groesser obere Grenze"
X_Unit_Diag_Area_End

X_Unit_Diag_Area=384-385
X_Value(0)  = "S:Pos1 im Arbeitsbereich"
X_Value(1)  = "S:Pos1 kleiner untere Grenze"
X_Value(2)  = "S:Pos1 groesser obere Grenze"
X_Unit_Diag_Area_End

X_Unit_Diag_Area=422-423
X_Value(0)  = "S:Spd4 im Arbeitsbereich"
X_Value(1)  = "S:Spd4 kleiner untere Grenze"
X_Value(2)  = "S:Spd4 groesser obere Grenze"
X_Unit_Diag_Area_End

X_Unit_Diag_Area=420-421
X_Value(0)  = "S:Spd3 im Arbeitsbereich"
X_Value(1)  = "S:Spd3 kleiner untere Grenze"
X_Value(2)  = "S:Spd3 groesser obere Grenze"
X_Unit_Diag_Area_End

X_Unit_Diag_Area=418-419
X_Value(0)  = "S:Spd2 im Arbeitsbereich"
X_Value(1)  = "S:Spd2 kleiner untere Grenze"
X_Value(2)  = "S:Spd2 groesser obere Grenze"
X_Unit_Diag_Area_End

X_Unit_Diag_Area=416-417
X_Value(0)  = "S:Spd1 im Arbeitsbereich"
X_Value(1)  = "S:Spd1 kleiner untere Grenze"
X_Value(2)  = "S:Spd1 groesser obere Grenze"
X_Unit_Diag_Area_End
EndUnitDiagType

Max_User_Prm_Data_Len = 114

;
;---modules---
;

Module= "Standardtelegramm 81" 0xc3, 0xc1, 0xc5, 0xfd, 0x00, 0x51
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x08,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1    ; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2    ; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3    ; Preset Control
Ext_User_Prm_Data_Ref(4)  =4    ; Scaling
Ext_User_Prm_Data_Ref(4)  =103  ; Diag
Ext_User_Prm_Data_Ref(5)  =12   ; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
0
EndModule

Module= "1 Positionswert" 0xe1, 0xd5, 0xd1
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x08,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
1
EndModule

Module= "2 Positionswerte" 0xe1, 0xd5, 0xd3
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x10,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
2
EndModule

Module= "3 Positionswerte" 0xe1, 0xd5, 0xd5
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x18,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
3
EndModule

Module= "4 Positionswerte" 0xe1, 0xd5, 0xd7
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x20,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
4
EndModule

Module= "1 Positions-/Geschw.wert" 0xe1, 0xd5, 0xd3
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x09,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
5
EndModule

Module= "2 Positions-/Geschw.werte" 0xe1, 0xd5, 0xd7
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x11,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
6
EndModule

Module= "3 Positions-/Geschw.werte" 0xe1, 0xd5, 0xdb
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x19,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
7
EndModule

Module= "4 Positions-/Geschw.werte" 0xe1, 0xd5, 0xdf
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x21,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
8
EndModule

Module= "FMM Positionswerte" 0xe1, 0xd5, 0xd7
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
Ext_User_Prm_Data_Ref(38) =11 ;Diag Time FMM
9
EndModule


Module= "FMM Positions-/Geschw.werte" 0xe1, 0xd5, 0xdf
Ext_Module_Prm_Data_Len= 39
Ext_User_Prm_Data_Const(0)=\
0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
0x00,\
0x12,0x00,0x00,0x00,0x01,0x00,0x00,0x03,0xe8,0x00,\
0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
Ext_User_Prm_Data_Ref(4)  =1	; Code Sequence
Ext_User_Prm_Data_Ref(4)  =2	; Class 4 Functionality
Ext_User_Prm_Data_Ref(4)  =3	; Preset Control
Ext_User_Prm_Data_Ref(4)  =4	; Scaling
Ext_User_Prm_Data_Ref(4)  =103	; Diag
Ext_User_Prm_Data_Ref(5)  =12	; Measuring Units Per Resolution
Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
Ext_User_Prm_Data_Ref(38) =11 ;Diag Time FMM
10
EndModule

;only for certification
;Module= "DPV2 Test CFG" 0xc3, 0xc1, 0xc6, 0xfd, 0x00, 0x64
;Ext_Module_Prm_Data_Len= 39
;Ext_User_Prm_Data_Const(0)=\
;0x15,0x81,0x00,0x00,0x02,0x00,0x00,0x03,0xe8,0x00,\
;0x00,0x13,0x88,0x0a,0x02,0x00,0x00,0x00,0x00,0x00,\
;0x00,\
;0x12,0x00,0x00,0x00,0x08,0x00,0x00,0x03,0xe8,0x00,\
;0x00,0x00,0x0A,0x00,0x00,0x00,0x0a,0x55
;Ext_User_Prm_Data_Ref(4)  =1    ; Code Sequence
;Ext_User_Prm_Data_Ref(4)  =2    ; Class 4 Functionality
;Ext_User_Prm_Data_Ref(4)  =3    ; Preset Control
;Ext_User_Prm_Data_Ref(4)  =4    ; Scaling
;Ext_User_Prm_Data_Ref(4)  =103  ; Diag
;Ext_User_Prm_Data_Ref(5)  =12   ; Measuring Units Per Resolution
;Ext_User_Prm_Data_Ref(9)  =6 ;Total Measuring Range
;Ext_User_Prm_Data_Ref(13) =20 ;max. tol.failures of lifesign
;Ext_User_Prm_Data_Ref(14) =21 ;velocity measuring unit
;Ext_User_Prm_Data_Ref(26) =8 ;Pos. Engineering Unit [0.001um]
;Ext_User_Prm_Data_Ref(30) =9 ;Speed Engineering Unit [0.01mm]
;Ext_User_Prm_Data_Ref(34) =10 ;Acc. Resoltion Unit [0.01mm/s2]
;Ext_User_Prm_Data_Ref(38) =11 ;Diag Time FMM
;11
;EndModule


Ext_User_Prm_Data_Const(0) = 0x00,0x00,0x08; constant values for ASIC
Modul_Offset        =0





















